#!/usr/bin/python3

import sys
from PyQt5.QtWidgets import (QMainWindow, QTabWidget, QWidget, QToolTip, QPushButton, QApplication, QDesktopWidget, QFormLayout, QLineEdit, QListWidget, QListWidgetItem)
from PyQt5.QtGui import QIcon, QFont

from mastodon import Mastodon

# Register app with server (only needed the first time)
'''
print('Registering app...')
Mastodon.create_app(
    'Mahout',
    api_base_url = 'https://yourinstance.org',
    to_file = 'mahout_clientcred.secret'
)
'''

# Log-in (if no stored credential is avaliable)
'''
print('Logging-in...')
mastodon = Mastodon(
    client_id = 'mahout_clientcred.secret',
    api_base_url = 'https://yourinstance.org'
)
mastodon.log_in(
    'user@domain.com',
    'yourpassword',
    to_file = 'mahout_usercred.secret'
)
'''

# Create instance of Mastodon.py module
print('Initializing API client...')
mastodon = Mastodon(
    client_id = 'mahout_clientcred.secret',
    access_token = 'mahout_usercred.secret',
    api_base_url = 'https://social.coop'
)

class Mahout(QTabWidget):
    def __init__(self, parent= None):
        super(Mahout, self).__init__(parent)

        #self.last_toot = -1

        self.my_timeline_tab = QWidget()
        self.local_timeline_tab = QWidget()
        self.federated_timeline_tab = QWidget()
        self.notifications_tab = QWidget()
        self.profile_tab = QWidget()

        self.addTab(self.my_timeline_tab,'Timeline')
        self.addTab(self.local_timeline_tab, 'Local')
        self.addTab(self.federated_timeline_tab, 'Federated')
        self.addTab(self.notifications_tab, 'Notifications')
        self.addTab(self.profile_tab, 'Profile')

        self.my_timeline_UI()
        # TODO: init the rest of these 
        #self.local_timeline_UI()
        #self.federated_timeline_UI()
        #self.notifications_tab_UI()
        #self.profile_tab_UI()

        self.setWindowTitle('Mahout')
        self.setGeometry(100,100,500,550)
        self.setTabPosition(1)


    # load moar toots
    def get_toots(self):

        # load toots since the last toot displayed
        # (if no toots are displayed, load the last 10)
        if self.my_timeline_list.count() > 0:
            # TODO: load toots since last displayed toot (for now just reload last 10)
            print('Loading toots since ')
            home_timeline_toots = mastodon.timeline_home(limit=10)
        else:
            # load the last 10 toots
            print('Loading initial toots...')
            home_timeline_toots = mastodon.timeline_home(limit=10)

        # add toots to list
        for toot in home_timeline_toots:
            toot_summary = toot['account']['username'] + ': ' + toot['content']
            QListWidgetItem(toot_summary, self.my_timeline_list)

    # Toot event handler
    def send_toot(self):
        # debug
        print(self.message_edit.text())
        mastodon.toot(self.message_edit.text())
        self.get_toots()

    def my_timeline_UI(self):

        self.message_edit = QLineEdit()
        toot_btn = QPushButton('TOOT', self)
        toot_btn.clicked.connect(self.send_toot)
        self.my_timeline_list = QListWidget()

        self.get_toots()

        # stub (a dub) 
        #tl1 = QListWidgetItem('foo', my_timeline_list)
        #tl2 = QListWidgetItem('bar', my_timeline_list)
        #tl3 = QListWidgetItem('baz', my_timeline_list)

        layout = QFormLayout()
        layout.addRow('Message',self.message_edit)
        layout.addRow(toot_btn)
        layout.addRow(self.my_timeline_list)
        self.setTabText(0,'Timeline')
        self.my_timeline_tab.setLayout(layout)

def main():
    app = QApplication(sys.argv)
    mahout = Mahout()
    mahout.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
